import React from 'react';
import "@testing-library/jest-dom";
import {screen,render, fireEvent} from "@testing-library/react";
import App from "../src/App";
const util = require("util");
const sleep = util.promisify(setTimeout);


describe('LoginFrom',()=>{
    it("test sing up link works",()=>{
        render(<App />);
        fireEvent.click( screen.queryByText("Registrarse") );
        expect(screen.queryByRole('heading',{name:/Registrarse/i})).toBeInTheDocument();
    });

    it("test login", async ()=>{
        render(<App />);
        
        let inputEmail = screen.getByPlaceholderText("Email");
        let inputPassword = screen.getByPlaceholderText("Contraseña");

        fireEvent.change(inputEmail, 
            {
                target:{value:'pepe@gmail.com'
            }
        });
        fireEvent.change(inputPassword, 
            {
                target:{value:'12345678'
            }
        });
        
        await fireEvent.click( screen.queryByText("Iniciar sesión"));
        expect(screen.getByText("Crypto Rules")).toBeInTheDocument();
    });
})


