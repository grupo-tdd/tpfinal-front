# CryptoRules - Frontend

MVP para practicar la compra y venta de criptomonedas con la API de prueba de Binance, utilizando reglas serializadas en JSON. 

Se trata de una aplicación web, implementada a partir del patrón de arquitectura Enterprise Application (o Cliente-Servidor)

![](media/arq.png)

Log-in integrado con firebase, token de seguridad de 1 hora con jwt, posibilidad de compartir reglas. 

### Endpoints básicos
- Crear regla: POST a /users/rule 
- Actualizar regla: PUT a /users/rule
- Eliminar una regla: DELETE a /users/rule
- Obtener reglas: GET a /users/rule
- Obtener wallet actual: GET a /users/wallet
- Historial de transacciones: GET a /users/allorders

### Tecnologías utilizadas
- Node.js (ver dependencias en package.json)
- PostgreSQL (con sequelize)
- Express
- React.js
- Docker

### Resumen de trabajo futuro
- Funcionalidad: poder implementar estrategias de reglas a partir de un estado de mercado
- Escalabilidad: migración a microservicios
- Disponibilidad: tener redundancias y respaldos
- Seguridad: cifrar conexiones y resisitir DoS, Man in the Middle, etc.
- Interfaz de usuario: migrar a material design
