const crypto = require('crypto');

function getHash(toHash) {
  return crypto.createHmac('md5', toHash).digest('hex');
}


function verifyJWTSession(res){
    console.log("JWT verify status", res.status)
    if ( res.status === 401 ){
      alert("Timeout: Expiro la sesión")
      throw new Error("autorización invalida")
    }
    return res;
}


function encodeQuery(query) {
  if (query !== undefined) {
    return encodeURI(query);
  }

  return query;
}

function decodeQuery(query) {
  if (query !== undefined) {
    return decodeURI(query);
  }

  return query;
}

exports.verifyJWTSession = verifyJWTSession;
exports.getHash = getHash;
exports.decodeQuery = decodeQuery;
exports.encodeQuery = encodeQuery;
