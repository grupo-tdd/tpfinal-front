import "../style/LoginForm.css";
import {Component} from 'react';

class AppForm extends Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
    }

    handleInput(event) {
        const {value, name} = event.target;
        this.setState({[name]: value});
    }

    checkServerAnswer(response) {
        if ( response['error'] !== undefined ){
            alert(response['error']);
        }
    }

    checkIfNotValidRule(strBody){
        JSON.parse(strBody);
        //const body = JSON.parse(strBody);
        //const requiredVariables = body.requiredVariables;
        //const strRule = JSON.stringify(body.rules);
        /* if ( requiredVariables.length === 0 && strRule.includes("VARIABLE") ){
           alert("La regla tiene variables requeridas no especificadas");
           return false;
       } */

       /*
        if ( requiredVariables.length !== 0 && strRule.includes("VARIABLE") ){
            for(let i = 0; i < requiredVariables.length ; i++){
                if ( ! strRule.includes(requiredVariables[i]) ){
                    alert("La variable requerida no esta en la regla que se va a crear");
                    return false;
                }
            }
        }
        */
        return true;
    }

}

export default AppForm;
