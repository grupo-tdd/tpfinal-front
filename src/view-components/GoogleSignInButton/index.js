import React, {Component} from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from "../../legacy/Firebase.js";
import MyFirebase from "../../legacy/MyFirebase";

const successfulGoogleLogin = () => {
    //Aca en realidad vamos a tener que pegarle al back con un auth de firebase especial
    //Le vamos a tener que pasar el firebase token y guardamos en el storage lo que nos devielva el back
    return process.env.REACT_APP_MY_APP;
}

const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: successfulGoogleLogin(),
    // We will display Google and Facebook as auth providers.
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ]
};

class SignInScreen extends Component {
  render() {
        return (
            <div>
                <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={
                    firebase.auth()
                } />
                <MyFirebase setToken={this.props.setToken}/>
            </div>
        );
    }
}

export default SignInScreen;