import {Component} from 'react';
import { withRouter } from 'react-router-dom';

class CloseButton extends Component{
    constructor(props){
        super(props);
        this.closeSession = this.closeSession.bind(this);
    }

    async closeSession(){
        this.props.closeSession();
        await this.props.history.push("/");
    }


    render(){
        return(
            <div>
                <button type="button" onClick={this.closeSession}>
                    Cerrar sesión
                </button>
            </div>
            
        );
    }

}

export default withRouter(CloseButton);