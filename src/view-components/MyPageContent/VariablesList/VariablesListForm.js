import React, {Component} from 'react';
import "../../../style/VariablesList.css";
const utils = require("../../../utils")

class VariablesListForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      variables: {},
      varTable: undefined
    };
    this.handleInput = this.handleInput.bind(this);
    this.getBodySetVariable = this.getBodySetVariable.bind(this);
    this.handleClickSetVariable = this.handleClickSetVariable.bind(this);
    this.handleClickDeleteVariable = this.handleClickDeleteVariable.bind(this);
    this.updateVarTable = this.updateVarTable.bind(this);
  }

  async componentDidMount(){
    this.updateVarTable();
  }

  async updateVarTable() {
    const variables = await fetch(
      process.env.REACT_APP_BASE_PATH + process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_USERS + process.env.REACT_APP_MY_VARIABLES,
      {
        headers:{'x-auth-token': this.props.userInfo["userId"]}
      })
      .then(response => utils.verifyJWTSession(response))
      .then(response => response.json())
      .catch(async(err) =>  {
        console.log(err.message);
        if ( err.message === "autorización invalida" ){
            await this.props.closeSession();
        }
      });
  
    this.setState({ variables:variables });
    const table = Object.keys(this.state.variables).map(varName => {
      return(
        <tr key={varName}>
          <th>{varName}</th>
          <th>{
          typeof this.state.variables[varName] === "string" ? 
          '"' + this.state.variables[varName] + '"' :
          this.state.variables[varName].toString()
          }</th>
        </tr>
      );
    });
    this.setState({varTable: table});
  }

  handleInput(event) {
    const {value, name} = event.target;
    this.setState({[name]: value});
  }

  getBodySetVariable() {
    console.log("El estado actual es: ", this.state);
    try {
      return {
        name: this.state.setVarName,
        value: JSON.parse(this.state.setVarValue)
      };
    } catch(e) {
      throw Error("El valor debe ser un numero, un booleano (true/false) o una cadena de texto entre comillas dobles (\")");
    }
  }

  async handleClickSetVariable(e){
    e.preventDefault();
    try {
      let varBody = JSON.stringify(this.getBodySetVariable());
      await fetch(process.env.REACT_APP_BASE_PATH +
        process.env.REACT_APP_NODE_DOCKER_PORT +
        process.env.REACT_APP_USERS + 
        process.env.REACT_APP_MY_VARIABLES,{
        method: "PUT",
        headers:{ 
          'Content-Type': 'application/json',
          'x-auth-token': this.props.userInfo["userId"]
        },
        body: varBody})
        .then(res => res.json())
        .then(res => {
          if (res["error"])
            alert(res["error"].toString());
          console.log(res)
        })
        .catch(err => console.log(err));
      await this.updateVarTable();
    } catch(e) {
      alert("Error: " + (e.message).toString());
    } finally {
      document.getElementById("label-set-variable-name").value = "";
      document.getElementById("label-set-variable-value").value = "";
      this.setState({ setVarname:"", setVarValue: "" });
    }
    
  }

  async handleClickDeleteVariable(e){
    e.preventDefault();
    //console.log("Quiero borrar esta variable:", this.state.deleteVarName, "que tiene este tipo:", typeof this.state.deleteVarName);
    await fetch(process.env.REACT_APP_BASE_PATH +
      process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_USERS + 
      process.env.REACT_APP_MY_VARIABLES,{
      method: "DELETE",
      headers:{ 
        'Content-Type': 'application/json',
        'x-auth-token': this.props.userInfo["userId"]
      },
      body: JSON.stringify({ name: this.state.deleteVarName })})
      .then(res => res.json())
      .then(res => console.log(res))
      .catch(err => console.log(err))
      .finally(() => {
        document.getElementById("label-delete-variable-name").value = "";
        this.setState({ deleteVarName:"" });
      });
    await this.updateVarTable();
  }

  render() {
    return (
      <div>
        <div>
          <h1>Variables</h1>
        </div>

        <div className="variable-table">
          <table>
            <tr key={"JK34SLF21$2JLF"}>
              <th>Nombre</th>
              <th>Valor</th>
            </tr>
              {this.state.varTable}
          </table>
        </div>

        <div>
          <h3>
            Crear/actualizar variable
          </h3>
          <input 
            type='text'
            name='setVarName'
            id="label-set-variable-name"
            className='form-control' 
            placeholder="Nombre"
            onChange={this.handleInput}
            required>
          </input>
          <input
            type='text'
            name='setVarValue'
            id="label-set-variable-value"
            className='form-control' 
            placeholder="Valor"
            onChange={this.handleInput}
            required>
          </input>
          <br>
          </br>
          <br>
          </br>
          <button onClick={this.handleClickSetVariable}>
            Crear/actualizar
          </button>
        </div>

        <div>
          <h3>
            Eliminar variable
          </h3>
          <input 
            type='text'
            name='deleteVarName'
            id="label-delete-variable-name"
            className='form-control' 
            placeholder="Nombre"
            onChange={this.handleInput}
            required>
          </input>
          <br>
          </br>
          <br>
          </br>
          <button onClick={this.handleClickDeleteVariable}>
            Borrar
          </button>
        </div>

      </div>
    );
  }
}

export default VariablesListForm;