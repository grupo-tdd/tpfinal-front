import React, {Component} from 'react';
const utils = require("../../../utils");

class HomePage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            user:{}
        };

        this.userInfo = this.getUserInfo.bind(this);
    }

    componentDidMount() {
        this.getUserInfo();
    }

    async getUserInfo() {
        const {userInfo} = this.props;
        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_USER_INFO,
            { 
                headers: { 'x-auth-token': userInfo["userId"] }
             })
            .then( response => utils.verifyJWTSession(response) )
            .then(response => response.json())
            .then(data => this.setState({ user: data }))
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
    }

    render() {
        const {user} = this.state;

        return(
            <div>
                <h1 className="welcome-title">
                    Datos
                </h1>

                <ul className="user-list">
                    <li>Nombre: {user['name']}</li>
                    <li>Apellido: {user['surname']}</li>
                    <li>Mail: {user['email']}</li>
                </ul>

            </div>
        );
    }
}

export default HomePage;