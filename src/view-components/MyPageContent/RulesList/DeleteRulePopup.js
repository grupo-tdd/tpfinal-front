import { Component } from 'react';
import Popup from 'reactjs-popup';
import "../../../style/DeleteRulePopup.css";
const utils = require("../../../utils");

class DeleteRulePopup extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    async handleClick(e){
        e.preventDefault();
        const reqBody = { rule: this.props.ruleName};
        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_RULE,{
            method: "DELETE",
            headers:{ 
                'Content-Type': 'application/json',
                'x-auth-token': this.props.userId["userId"]
             },
            body: JSON.stringify(reqBody)})
            .then(response => utils.verifyJWTSession(response))
            .then( res => res.json())
            .then( res => console.log(res))
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
        
        await this.props.getRules();
    }


    render() {
        return(<div>
                    <Popup 
                        trigger={<button>Eliminar</button>}
                        position="left top center"
                        nested>
                    { close =>
                    <div className='delete-rule-popup'>
                        <button className="close" onClick={close}>
                            &times;
                        </button>
                        <h5>
                            Eliminar Regla
                        </h5>
                        <h6>
                            ¿Esta seguro que quiere eliminar la regla {this.props.ruleName}?
                        </h6>
                        <button onClick={(event)=>{this.handleClick(event);close();}}>
                            Aceptar
                        </button>
                    </div>
                    }
                    </Popup>
                </div>
        );
    }
}

export default DeleteRulePopup;