import RuleCard from './RuleCard'
import RuleList from "./RuleList";

class RulesListForm extends RuleList {
    render() {
        const {userInfo} = this.props;
        const rules = this.state.rules.map( (rule,i) => {
            return(
                <RuleCard
                    key={rule.name}
                    ruleId={i}
                    userId={userInfo} 
                    ruleName={rule.name}
                    executed={rule.executed}
                    getRules={this.getRules}
                    editor={rule.editor}/>
            );
        });

        return(
            <div>
                <nav>
                    <h6>Cantidad de reglas: {this.state.rules.length}</h6>
                </nav>
                <h1 className="welcome-title">
                    MIS REGLAS:
                </h1>
                {rules}            
            </div>
        );
    }

}

export default RulesListForm;