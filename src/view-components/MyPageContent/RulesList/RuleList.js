import {Component} from "react";
const utils = require("../../../utils");

class RuleList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rules: []
        };

        this.getRules = this.getRules.bind(this);
    }

    async componentDidMount(){
        await this.getRules();
    }

    async getRules() {
        const {userInfo} = this.props;
        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_RULE,
            {
                headers: {'x-auth-token': userInfo["userId"]}
            }
            ).then(response => utils.verifyJWTSession(response))
            .then(response => response.json())
            .then(response => response.rules)
            .then(response => this.setState({ rules: response }))
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
    }
}

export default RuleList;
