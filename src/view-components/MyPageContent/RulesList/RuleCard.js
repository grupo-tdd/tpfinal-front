import "../../../style/RuleCard.css";
import AppForm from './../../AppForm';
import ShareRulePopup from "./ShareRulePopup";
import DeleteRulePopup from "./DeleteRulePopup";
import UpdateRulePopup from "./UpdateRulePopup";

class RuleCard extends AppForm {
    constructor(props) {
        super(props);
        this.state = {
            ruleName: "",
            executed: 0,
            checked: false
        };

    }

    componentDidMount() {
        this.setState({
            ruleName: this.props.ruleName, executed: this.props.executed, editor: this.props.editor
        })
    }

    render() {
        return(<div className='rule-card'>
                    <div className="rule-info">
                        <h1 className='rule-tittle'>
                            {this.state.ruleName}
                        </h1>
                        <h2 className='rule-tittle'>
                            Cantidad de veces ejecutada: {this.state.executed.toString()}
                        </h2>
                        <h2 className='rule-permission'>
                            Permisos: {(this.state.editor)? "Editor":"Solo Lectura"}
                        </h2>
                    </div>
                        <div classname="rule-share-button">
                            <ShareRulePopup userId={this.props.userId} ruleName={this.props.ruleName} editor={this.props.editor}/>
                        </div>
                        <div className="rule-update-button">
                            <UpdateRulePopup userId={this.props.userId} ruleName={this.props.ruleName} getRules={this.props.getRules} editor={this.props.editor}/>
                        </div>
                        <div className="rule-delete-button">
                            <DeleteRulePopup userId={this.props.userId} ruleName={this.props.ruleName} getRules={this.props.getRules}/>
                        </div>
                </div>
        );
    }
}

export default RuleCard;
