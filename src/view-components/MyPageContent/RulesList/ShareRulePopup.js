import Popup from 'reactjs-popup';
import AppForm from '../../AppForm';
import "../../../style/ShareRulePopup.css";
const utils = require("../../../utils");


class ShareRulePopup extends AppForm {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
        this.getBody = this.getBody.bind(this);
    }

    getBody(){
        let permission = "";

        if ( this.state.checked ){
            permission = "editor";
        }
        else{
            permission = "read only";
        }

        return {
            hostId: this.props.userId["userId"],
            guestEmail: this.state.email,
            rule: this.props.ruleName,
            permission: permission
        }
    }

    async handleClick(e){
        e.preventDefault();

        if ( ( ! this.props.editor) && (this.state.checked) ){
            alert("No tienes permiso para compartir esta regla como Editor");
            return;
        }
        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_USERS + "/share_rule",{
            method: "POST",
            headers:{ 
                'Content-Type': 'application/json',
                'x-auth-token': this.props.userId["userId"]
             },
            body: JSON.stringify(this.getBody())})
            .then(response => utils.verifyJWTSession(response))
            .then( res => res.json())
            .then( res => {
                if ( res.error === "Ya existe una regla con ese nombre" ){
                    alert(`El usuario ${this.state.email} ya tiene una regla con el nombre ${this.props.ruleName}`)
                }
            })
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
    }

    handleCheck() {
        this.setState({checked: !this.state.checked});
      }


    render() {
        return(<div >
                    <Popup 
                        trigger={<button>Compartir</button>}
                        position="left top center"
                        nested>
                    { close =>
                    <div className="share-rule-popup">
                        <button className="close" onClick={close}>
                            &times;
                        </button>
                        <h5>
                            Compartir Regla
                        </h5>
                        
                        <input 
                            type='email'
                            name='email'
                            id="label-share-rule-email"
                            className='form-control' 
                            placeholder="Email"
                            onChange={this.handleInput}
                            required>
                        </input>
                        <input 
                            type="checkbox"
                            name="permissions"
                            id="checkbox-share-rule"
                            value="Editor"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}>
                            
                        </input>
                        <label for="permissions">Permiso de Editor</label>
                        <br>
                        </br>
                        <button onClick={(event)=>{this.handleClick(event);close();}}>
                            Aceptar
                        </button>
                    </div>
                    }
                    </Popup>
                </div>
        );
    }
}

export default ShareRulePopup;