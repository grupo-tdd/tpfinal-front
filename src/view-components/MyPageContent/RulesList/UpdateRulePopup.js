import Popup from 'reactjs-popup';
import AppForm from '../../AppForm';
import "../../../style/UpdateRulePopup.css";
const utils = require("../../../utils");

class UpdateRulePopup extends AppForm {
    constructor(props) {
        super(props);
        this.state = {
            updatedFile: {},
            originalFile:{}
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleInputTextARea = this.handleInputTextARea.bind(this);
    }

    async componentDidMount() {
        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_RULE +
            process.env.REACT_APP_FILE +
            "?ruleName="+ utils.encodeQuery(this.props.ruleName),
            {
                headers: {'x-auth-token': this.props.userId["userId"]}
            })
            .then(response => utils.verifyJWTSession(response))
            .then(response => response.json())
            .then(response => 
                {
                    if (response.error !== undefined) {
                        alert(response.error);
                    };
                    this.setState({
                        originalFile: JSON.stringify(response.file,undefined, 2),
                        updatedFile: JSON.stringify(response.file,undefined, 2)
                     })
                })
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
    }

    async handleClick(e){
        e.preventDefault();
        if ( ! this.props.editor ){
            alert("No tienes permiso para alterar esta regla");
            return;
        }
        let file = undefined;
        try {
            file = JSON.parse(this.state.updatedFile);
        } catch(e) {
            alert("Parseo de regla invalido, formato de JSON incorrecto: " + e.message);
            return;
        }
        file["name"] = this.props.ruleName;
        console.log(this.props.ruleName);

        if ( ! this.checkIfNotValidRule(JSON.stringify(file)) ){
            return;
        }

        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_RULE,
            {
                method: "PUT",
                headers: {  'x-auth-token': this.props.userId["userId"],
                            'Content-Type': 'application/json'},
                body: JSON.stringify(file)
            }
        )
        .then(response => utils.verifyJWTSession(response))
        .then(response => response.json())
        .then(response => {
            if (response.error !== undefined)
                alert(response.error);
        })
        .catch(async(err) =>  {
            console.log(err.message);
            if ( err.message === "autorización invalida" ){
                await this.props.closeSession();
            }
        });
        console.log(file);   

        await this.props.getRules();
    }

    handleInputTextARea(event) {
        const {value } = event.target;
        this.setState({updatedFile:value});
    }

    resetFile(){
        this.setState({updatedFile:this.state.originalFile});
    }

    render() {
        return(<div >
                    <Popup 
                        trigger={<button>Editar</button>}
                        position="left top center"
                        closeOnDocumentClick
                        closeOnEscape
                        nested>
                    { close =>
                    <div className="update-rule-popup">
                        <button className="close" onClick={()=>{close();this.resetFile();}}>
                            &times;
                        </button>
                        <h5>
                            Editar Regla: {this.props.ruleName}
                        </h5>
                        <textarea name="file" onChange={this.handleInputTextARea}>
                            {this.state.updatedFile}
                        </textarea>
                        
                        <button onClick={(event)=>{this.handleClick(event);close();this.resetFile();}}>
                            Aceptar
                        </button>
                    </div>
                    }
                    </Popup>
                </div>
        );
    }
}

export default UpdateRulePopup;