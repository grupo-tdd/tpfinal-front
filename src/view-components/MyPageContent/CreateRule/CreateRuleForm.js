import AppForm from '../../AppForm';
const utils = require('../../../utils');
require('../../../style/Rule.css');

class CreateRuleForm extends AppForm {
  constructor() {
    super();
    this.state = {body: {}};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.doDummyGet = this.doDummyGet.bind(this);
  }

  componentDidMount() {
    this.doDummyGet();
  }

  async doDummyGet() {
    const {userInfo} = this.props;
    await fetch(process.env.REACT_APP_BASE_PATH +
      process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_USER_INFO,
      { 
        headers: { 'x-auth-token': userInfo["userId"] }
      })
      .then( response => {utils.verifyJWTSession(response)} )
      .catch(async(err) =>  {
        console.log(err.message);
        if ( err.message === "autorización invalida" ){
          await this.props.closeSession();
        }
      });
    }

  handleInput(event) {
    const {value, name} = event.target;
    this.setState({body: Object.assign(this.state.body, {[name]: value})});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {userInfo} = this.props;

    let strBody = this.state.body.ruleinput;
    try {
      if ( ! this.checkIfNotValidRule(strBody) ){
        return;
      }
    } catch(e) {
      alert("Parseo de regla invalido, formato de JSON incorrecto: " + e.message);
      return;
    }
    
    
    console.log("POST:"+process.env.REACT_APP_BASE_PATH +
    process.env.REACT_APP_NODE_DOCKER_PORT +
    process.env.REACT_APP_RULE);

    await fetch(process.env.REACT_APP_BASE_PATH +
      process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_RULE, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'x-auth-token': userInfo["userId"]
      },
      body: strBody
    })
      .then( response => utils.verifyJWTSession(response) )
      .then(response => response.json())
      .then(response => {
        if (response.error !== undefined) {
          alert(response.error);
        } else {
          alert(response.body);
          document.getElementById("rule-input-area").value = "";
        }
      })
     .catch(async(err) =>  {
      console.log(err.message);
      if ( err.message === "autorización invalida" ){
        await this.props.closeSession();
      }
    });
  }

  render() {
    return (
      <div className='create-rule'>
        <form onSubmit={this.handleSubmit}>
          <div>
          <textarea id='rule-input-area'
                    name='ruleinput'
                    className='ruleinput'
                    onChange={this.handleInput}>
        </textarea>
          </div>

          <div>
            <br>
            </br>
          </div>

          <div>
            <button type="submit">
              Crear/actualizar
            </button>
          </div>
        </form>
      </div>
    );
  }

}

export default CreateRuleForm;