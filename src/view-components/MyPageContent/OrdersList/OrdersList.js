import "../../../style/OrdersList.css";
import AppForm from "../../AppForm";
import moment from 'moment-timezone';
const utils = require("../../../utils");

class OrdersList extends AppForm {
    constructor(props){
        super(props);
        this.state = {
            orders: []
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(e){
        e.preventDefault();

        await fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_USERS +
            process.env.REACT_APP_ALL_ORDERS+"?symbol="+this.state.symbol,
            { 
                headers: { 'x-auth-token': this.props.userInfo["userId"] }
             })
            .then(response => utils.verifyJWTSession(response) )
            .then(response => response.json())
            .then(response => {
                this.setState({orders: response.allOrders})
            })
            .then(()=>{document.getElementById("form-control-symbol-field").value=""})
            .catch(async(err) =>  {
                console.log(err.message);
                if ( err.message === "autorización invalida" ){
                    await this.props.closeSession();
                }
            });
    }

    render(){
        const orders = this.state.orders.reverse().map((anOrder,i) =>{
            return(
            <div className="order-card" key={i}>
                <h6>Id orden: {anOrder.orderId}</h6>
                <h6>Id orden del cliente: {anOrder.clientOrderId}</h6>
                <h6>Precio: {anOrder.price}</h6>
                <h6>Cantidad solicitada: {anOrder.origQty}</h6>
                <h6>Cantidad ejecutada: {anOrder.executedQty}</h6>
                <h6>Status: {anOrder.status}</h6>
                <h6>Tipo de acción: {anOrder.side} {anOrder.type}</h6>
                <h6>Tiempo actualización: {moment(anOrder.updateTime).tz("America/Buenos_Aires").format("YYYY MMM DD HH:mm:ss")}</h6>
            </div>);
        });

        let title;
        if ( orders.length !== 0 ){
            title = () => {return (<h4>state.symbol</h4>)}
        }

        return(
        <div>
        <form onSubmit={this.handleSubmit}>
          <h1 className="app-name"><i>Historial de Ordenes Realizadas</i></h1>

          <div>
            <input
              type='text'
              name='symbol'
              placeholder={`Ej: BNBUSDT`}
              id='form-control-symbol-field'
              className='form-control-symbol-field-input'
              onChange={this.handleInput}
            >
            </input>
          </div>

          <div>
            <br>
            </br>
          </div>

          <div>
            <button type="submit" className="form-control-submit-symbol">
              Solicitar Historial
            </button>
          </div>
            
        <div>
            {title}
            {orders}
        </div>  
        </form>
        </div>)
    }
        

}

export default OrdersList;