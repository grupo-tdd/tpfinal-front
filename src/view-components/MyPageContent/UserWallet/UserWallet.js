import React, {Component} from 'react';
import "../../../style/UserWallet.css";
const utils = require("../../../utils")

class UserWallet extends Component {
  constructor(props){
    super(props);
    this.state = {
        wallet: []
    };
  }

  async componentDidMount(){
    const balances = await fetch(
        process.env.REACT_APP_BASE_PATH + process.env.REACT_APP_NODE_DOCKER_PORT +
        process.env.REACT_APP_USERS + process.env.REACT_APP_WALLET_QUERY,
        {
            headers:{'x-auth-token': this.props.userInfo["userId"]}
        })
        .then( response => utils.verifyJWTSession(response) )
        .then(response => response.json())
        .then(response => {return response.balances})
        .catch(async(err) =>  {
            console.log(err.message);
            if ( err.message === "autorización invalida" ){
                await this.props.closeSession();
            }
        });
    
    if (balances === undefined)
        return;
    
    this.setState({wallet:balances});
  }

  render() {
        const table = this.state.wallet.map( crypto => 
            {
                return(
                <tr key={crypto.asset}>
                    <th>{crypto.asset}</th>
                    <th>{crypto.free}</th>
                </tr>);
            } );

        return (
            <div>
                <div>
                    <h1>CARTERA</h1>
                </div>
                <div className="wallet-table">
                    <table>
                        <tr>
                            <th>Moneda</th>
                            <th>Cantidad</th>
                        </tr>
                        {table}
                    </table>
                </div>

               
            </div>
        );
    }
}

export default UserWallet;