import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import HomePage from './HomePage/HomePage';
import UpdateUserForm from './UpdateUser/UpdateUserForm';
import UserWallet from './UserWallet/UserWallet';
import CreateRuleForm from './CreateRule/CreateRuleForm';
import RulesListForm from './RulesList/RulesListForm';
import OrdersList from './OrdersList/OrdersList'
import CloseButton from './CloseButton';
import VariablesListForm from './VariablesList/VariablesListForm'
import "../../style/HomePageRoutes.css";

class HomePageRoutes extends Component{
    
    render(){
        const pathToMyApp = process.env.REACT_APP_MY_APP;
        const pathToUpdate = process.env.REACT_APP_UPDATE;
        const pathToWallet = process.env.REACT_APP_MY_WALLET;
        const pathToCreateRule = process.env.REACT_APP_CREATE_RULE;
        const pathToMyRules = process.env.REACT_APP_MY_RULES;
        const pathToMyVariables = process.env.REACT_APP_MY_VARIABLES;
        const pathToMyOrdersHistory = process.env.REACT_APP_MY_ORDERS_HISTORY;
        return(
            <Router>
                <nav className="container">
                    <div>
                        <CloseButton closeSession={this.props.closeSession} />
                    </div>

                    <div className="links">
                        <Link className="homepage-link" to={pathToMyApp}>Inicio</Link>
                        <Link className="update-user-link" to={pathToUpdate}>Mis claves</Link>
                        <Link className="user-wallet-link" to={pathToWallet}>Cartera</Link>
                        <Link className="create-rule-link" to={pathToCreateRule}>Crear regla</Link>
                        <Link className="rules-list-link" to={pathToMyRules}>Mis reglas</Link>
                        <Link className="variables-list-link" to={pathToMyVariables}>Mis variables</Link>
                        <Link className="orders-list-link" to={pathToMyOrdersHistory}>Historial de transacciones</Link>
                    </div>
                </nav>

                <div>
                    <Switch>
                        <Route path={pathToMyApp}>
                            <HomePage userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToWallet}>
                            <UserWallet userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToUpdate}>
                            <UpdateUserForm userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToCreateRule}>
                            <CreateRuleForm userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToMyRules}>
                            <RulesListForm userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToMyVariables}>
                            <VariablesListForm userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path={pathToMyOrdersHistory}>
                            <OrdersList userInfo={this.props.userInfo} closeSession={this.props.closeSession}/>
                        </Route>

                        <Route path="/">
                            <Redirect push to={pathToMyApp}/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default HomePageRoutes;