import AppForm from '../../AppForm';
require('../../../style/KeyInput.css');
const utils = require("../../../utils")


class UpdateUserForm extends AppForm {
  constructor() {
    super();
    this.state = {body: {}};
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const keys = await fetch(
      process.env.REACT_APP_BASE_PATH +
      process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_USERS +
      process.env.REACT_APP_KEYS,
      {
        headers: {'x-auth-token': this.props.userInfo["userId"]}
      })
      .then(response => utils.verifyJWTSession(response) )
      .then(response => response.json())
      .catch(async(err) =>  {
        console.log(err.message);
        if ( err.message === "autorización invalida" ){
            await this.props.closeSession();
        }
      });

    this.setState({body: keys});
    console.log(this.state.body);
  }

  handleInput(event) {
    const {value, name} = event.target;
    let newBody = Object.assign(this.state.body, {[name]: value});

    if (value === "") {
      delete newBody[name];
    }

    this.setState({body: newBody});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {userInfo} = this.props;
    let {body} = this.state;

    await fetch(process.env.REACT_APP_BASE_PATH +
      process.env.REACT_APP_NODE_DOCKER_PORT +
      process.env.REACT_APP_USERS +
      process.env.REACT_APP_KEYS, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'x-auth-token': userInfo["userId"]
      },
      body: JSON.stringify(body)
    })
      .then(response => utils.verifyJWTSession(response))
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.checkServerAnswer(response);
      })
      .then(() => {
          document.getElementById("form-control-apikey-field").value = "";
          document.getElementById("form-control-apisecret-field").value = "";
        }
      ).catch(async(err) =>  {
        console.log(err.message);
        if ( err.message === "autorización invalida" ){
            await this.props.closeSession();
        }
    });
  }

  render() {
    let api_key = "";
    let api_secret = "";

    try {
      api_key = this.state.body.api_key;
      api_secret = this.state.body.api_secret;
    } catch(err) {
      api_key = "";
      api_secret = "";
    }

    return (
      <div className='user-update'>
        <form onSubmit={this.handleSubmit}>
          <h1 className="app-name"><i>Actualizar Credenciales</i></h1>

          <div>
            <input
              type='text'
              name='api_key'
              placeholder={`Api-key (Ej: ${api_key})`}
              id='form-control-apikey-field'
              className='key-input'
              onChange={this.handleInput}
            >
            </input>
          </div>

          <div>
            <br>
            </br>
          </div>

            <div>
            <input
              type='text'
              name='api_secret'
              id='form-control-apisecret-field'
              className='key-input'
              placeholder={`Api-secret (Ej: ${api_secret})`}
              onChange={this.handleInput}
            >
            </input>
          </div>

          <div>
            <br>
            </br>
          </div>

          <div>
            <button type="submit" className="form-control">
              Actualizar
            </button>
          </div>

        </form>
      </div>
    );
  }

}

export default UpdateUserForm;