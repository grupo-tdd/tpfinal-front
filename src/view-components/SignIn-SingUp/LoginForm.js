import React from 'react';
import "../../style/LoginForm.css";
import AppForm from "../AppForm";
import { withRouter } from 'react-router-dom';
import GoogleSignIn from "../../view-components/GoogleSignInButton";

const utils = require("../../utils.js");


class LoginForm extends AppForm {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        let requestBody = { email: this.state.email,
            password: utils.getHash(this.state.password) };

        fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_LOG_IN_PAGE,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body:JSON.stringify(requestBody)
        })
        .then(response => response.json())
        .then(response => {
                this.props.setToken(response);
                this.checkServerAnswer(response);
            });

    }

    render(){
        return(
            <div className="page-body">
                <div className="login-card">
                    <h1 className="app-name"><i>Crypto Rules</i></h1>
                    <form className="login-form" onSubmit={this.handleSubmit}>
                        <div>
                            <input 
                                type='email'
                                name='email'
                                id="label-email"
                                className='form-control' 
                                placeholder="Email"
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <input 
                                type='password'
                                name='password'
                                className='form-control'
                                id="label-password"
                                placeholder="Contraseña"
                                minLength="8"
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <button type="submit" className="form-control">
                                Iniciar sesión
                            </button>  
                        </div>

                        <div>
                            <GoogleSignIn setToken={this.props.setToken}/>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(LoginForm);