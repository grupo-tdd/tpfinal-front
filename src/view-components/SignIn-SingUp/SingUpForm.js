import React from 'react';
import { withRouter } from 'react-router-dom';
import AppForm from "../AppForm";

const utils = require("../../utils.js");

class SingUpForm extends AppForm {
    constructor(){
        super();
        this.state = {
            name: '',
            surname: '',
            email: '',
            password: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault()

        let requestBody = {
            name: this.state.name,
            surname: this.state.surname,
            email: this.state.email,
            password: utils.getHash(this.state.password) };

        fetch(process.env.REACT_APP_BASE_PATH +
            process.env.REACT_APP_NODE_DOCKER_PORT +
            process.env.REACT_APP_SIGN_UP,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body:JSON.stringify(requestBody)
        })
        .then( response => response.json() )
        .then(response => {
            if ( response["email"] !== undefined ){
                this.props.history.push("/");
            }

            this.checkServerAnswer(response);
        });
    }

    render(){
        return(
            <div className="page-body">
                <div className="login-card">
                    <h1 className="app-name"><i>Crypto Rules</i></h1>
                    <form className="login-form" onSubmit={this.handleSubmit}>
                        <div>
                            <h1> Registrarse </h1>
                        </div>

                        <div>
                            <input
                                type='text'
                                name='name'
                                placeholder="Nombre"
                                className='form-control'
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <input
                                type='text'
                                name='surname'
                                className='form-control'
                                placeholder="Apellido"
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <input
                                type='email'
                                name='email'
                                className='form-control'
                                placeholder="Email"
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <input
                                type='password'
                                name='password'
                                className='form-control'
                                placeholder="Contraseña"
                                minLength="8"
                                onChange={this.handleInput}
                                required>
                            </input>
                        </div>

                        <div>
                            <button type="submit" className="form-control">
                                Registrarse
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(SingUpForm);
