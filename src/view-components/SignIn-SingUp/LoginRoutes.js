import "../../style/LoginRoutes.css"
import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import LoginForm from './LoginForm';
import SingUpForm from './SingUpForm';

class LoginRoutes extends Component{
    render(){           
        return(
            <nav>
                <Router>
                    <div className="container">
                        <Link className="start-sesion-link" to="/signin">Iniciar sesión</Link>
                        <Link className="sign-up-link" to="/signup">Registrarse</Link>
                    </div>   

                    <Switch>
                        <Route path="/signup">
                            <SingUpForm />
                        </Route>
                        <Route path="/signin">
                            <LoginForm setToken={this.props.setToken}/>
                        </Route>

                        <Route path="/">
                            <Redirect push to="/signin"/>
                        </Route>
                    </Switch>
                </Router>
            </nav>
            
        );
    }

}

export default LoginRoutes;