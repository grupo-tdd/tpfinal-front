import AppForm from './AppForm'

class EmailAdder extends AppForm {
    constructor(){
        super()
        this.state = {
            item: ''
        }
        this.handleClick = this.handleClick.bind(this);
        this.addNewItem = this.addNewItem.bind(this);
    }

    handleClick() {
      const {item} = this.state;

      if (item === "" || this.props.items.includes(item))
        return;

      if (!item.includes("@") || !item.includes(".")) {
        alert(process.env.REACT_APP_INVALID_EMAIL);
        return
      }

      this.addNewItem(item);
    }

    addNewItem(item){
        this.props.addItem(item);
        this.setState({item: ''});
    }

    render(){          
        const items = this.props.items.map( item =>{
            return(<li key={item}>{item}</li>);
        } );

        return(
            <div className='email-adder'>
                <input
                    type='email'
                    name='item'
                    id="textField"
                    className='groups-form-control' 
                    placeholder="Email"
                    value = {this.state.item}
                    onChange={this.handleInput}>
                </input>

                <button 
                    type='button'
                    className='groups-form-control'
                    onClick={this.handleClick} >
                        Agregar usuario
                </button>

                <ul className='added-list'>
                    {items}
                </ul>
            </div>
        );
    }

}

export default EmailAdder;
