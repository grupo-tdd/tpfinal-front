import React, {Component} from 'react';
import HomePageRoutes from "./view-components/MyPageContent/HomePageRoutes";
import LoginRoutes from './view-components/SignIn-SingUp/LoginRoutes';
import firebase from "./legacy/Firebase.js";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      logged: false,
      userID: {}
    };
    this.setToken = this.setToken.bind(this);
    this.closeSession = this.closeSession.bind(this);
  }

  setToken(response) {
    if ( response['userId'] !== undefined )  {
      console.log("Info: email and password correct, proceed to Homepage");
      this.setState({
          logged: true,
          userID: response
      }, ()=> localStorage.setItem("MyApp",JSON.stringify(this.state)));
    }
  }

  async closeSession(){
    await firebase.auth().signOut();
    this.setState({
      logged: false,
      userID: {}
    }, ()=> {
        localStorage.setItem("MyApp",JSON.stringify(this.state));
      });
    console.log("Session closed");
    }

  componentDidMount() {
    const storageLogged = JSON.parse(localStorage.getItem("MyApp"));
    if ( storageLogged ){
        this.setState(storageLogged);
    }
  }

  render() {
    if ( ! this.state.logged ) {
      return(
        <div className="App">
            <LoginRoutes setToken={this.setToken}/>
        </div>
      )
    }

    return (
        <div className="App">
            <HomePageRoutes userInfo={this.state.userID} closeSession={this.closeSession}/>
        </div>
    );
  }

}

export default App;
