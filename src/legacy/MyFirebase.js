import { removeValue } from "./LocalStorageService";
import firebase from "./Firebase.js";
import React, {Component} from 'react';

class MyFirebase extends Component {
  constructor(props){
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async handleChange(user) {
    if (user) {
      if (user.displayName){
        
        const token = JSON.parse(localStorage.getItem("MyApp"));
        if ( token && token["logged"] ){
          return;
        }
        
        const completeName = user.displayName.split(" ");
        const currentUser = {
          name: completeName[0],
          surname: completeName[2] ? completeName[1] + " " + completeName[2] :completeName[1],
          email: user.email,
          uid: user.uid
        };

        await fetch(process.env.REACT_APP_BASE_PATH +
          process.env.REACT_APP_NODE_DOCKER_PORT +
          process.env.REACT_APP_LOG_IN_GOOGLE, {
          method:"POST",
          headers: 
          { 'Content-Type': 'application/json'},
          body: JSON.stringify(currentUser)
        })
          .then(response => response.json())
          .then(res => {
            this.props.setToken(res);
          })
          .catch(err => {
            console.log(err);
          });
      }
    } else {
      removeValue("MyApp")
      console.log("Se cerró la sesión de google");
    }
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(this.handleChange);
  }

  render() {
    return ( <div> </div> );
  }
}

export default MyFirebase;
