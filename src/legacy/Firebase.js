import firebase from  'firebase';

const config = {
    apiKey: "AIzaSyBnxiDEgVqgKKx55HpIAP30PJtzrNSE7dc",
    authDomain: "tdd-server-e8da1.firebaseapp.com",
    projectId: "tdd-server-e8da1",
    storageBucket: "tdd-server-e8da1.appspot.com",
    messagingSenderId: "170818433827",
    appId: "1:170818433827:web:6dff9fc8f117f0fb455296",
    measurementId: "G-V37LKMSC8J"
};

firebase.initializeApp(config);

export default firebase;
